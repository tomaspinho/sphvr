/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "sphvr-pipeline.h"

typedef struct
{
  float mvp_matrix[2][16];
  float gamma;
  float unused;
  uint32_t stereo_mode;
  uint32_t phi_range;
} Transformation;

typedef struct _SphvrPipelinePrivate
{
  GObject parent;

  GulkanRenderer *renderer;

  GulkanVertexBuffer *vb;
  GulkanDescriptorPool *descriptor_pool;
  GulkanUniformBuffer *transformation_ubo;

  GulkanPipeline *pipeline;
  GulkanDescriptorSet *descriptor_set;

  Transformation ubo;

  bool image_view_received;

  sphvr_projection_type projection_type;
} SphvrPipelinePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (SphvrPipeline, sphvr_pipeline, G_TYPE_OBJECT)

static void
sphvr_pipeline_init (SphvrPipeline *self)
{
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);
  priv->image_view_received = false;
  priv->renderer = NULL;
  priv->pipeline = NULL;
  priv->descriptor_set = NULL;
}

static void
_finalize (GObject *gobject)
{
  SphvrPipeline *self = SPHVR_PIPELINE (gobject);
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);

  /* Check if rendering was initialized */
  if (priv->pipeline != NULL) {
    g_object_unref (priv->descriptor_pool);
    g_object_unref (priv->vb);
    g_object_unref (priv->pipeline);
    g_object_unref (priv->transformation_ubo);
  }

  g_clear_object (&priv->renderer);

  G_OBJECT_CLASS (sphvr_pipeline_parent_class)->finalize (gobject);
}

SphvrPipeline *
sphvr_pipeline_new (sphvr_projection_type projection_type, float gamma,
                    sphvr_stereo_mode stereo_mode, sphvr_φ_range φ_range)
{
  SphvrPipeline *self = NULL;

  switch (projection_type) {
    case SPHVR_PROJECTION_EQUIRECT:
      self = SPHVR_PIPELINE (g_object_new (SPHVR_TYPE_PIPELINE_PLANE, 0));
      break;
    case SPHVR_PROJECTION_CUBE:
    case SPHVR_PROJECTION_EAC:
      self = SPHVR_PIPELINE (g_object_new (SPHVR_TYPE_PIPELINE_CUBE, 0));
      break;
    default:
      g_printerr ("Unknown projection type %d\n", projection_type);
      g_assert ("UNREACHABLE" && FALSE);
      return NULL;
  }

  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);
  priv->ubo = (Transformation){
    .gamma = gamma,
    .stereo_mode = stereo_mode,
    .phi_range = φ_range,
  };
  priv->projection_type = projection_type;

  return self;
}

void
sphvr_pipeline_set_image_view (SphvrPipeline *self,
                               VkImageView texture_image_view,
                               VkSampler sampler)
{
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);

  g_assert (priv->descriptor_set != NULL);

  gulkan_descriptor_set_update_view_sampler (priv->descriptor_set, 1,
                                             texture_image_view, sampler);
  priv->image_view_received = true;
}

void
sphvr_pipeline_init_draw_cmd (SphvrPipeline *self, VkCommandBuffer cmd_buffer)
{
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);

  if (!priv->image_view_received)
    return;

  gulkan_pipeline_bind (priv->pipeline, cmd_buffer);

  VkPipelineLayout pipeline_layout
    = gulkan_descriptor_pool_get_pipeline_layout (priv->descriptor_pool);

  gulkan_descriptor_set_bind (priv->descriptor_set, pipeline_layout,
                              cmd_buffer);

  SphvrPipelineClass *klass = SPHVR_PIPELINE_GET_CLASS (self);
  if (klass->draw_cmd == NULL)
    return;

  klass->draw_cmd (priv->vb, cmd_buffer);
}

void
sphvr_pipeline_set_vp (SphvrPipeline *self, graphene_matrix_t *vp)
{
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);
  graphene_matrix_to_float (&vp[0], priv->ubo.mvp_matrix[0]);
  gulkan_uniform_buffer_update (priv->transformation_ubo,
                                (gpointer) &priv->ubo);
}

void
sphvr_pipeline_set_vps (SphvrPipeline *self, graphene_matrix_t *vp)
{
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);
  graphene_matrix_to_float (&vp[0], priv->ubo.mvp_matrix[0]);
  graphene_matrix_to_float (&vp[1], priv->ubo.mvp_matrix[1]);
  gulkan_uniform_buffer_update (priv->transformation_ubo,
                                (gpointer) &priv->ubo);
}

gboolean
sphvr_pipeline_initialize (SphvrPipeline *self, GulkanRenderer *renderer,
                           GulkanRenderPass *render_pass,
                           gboolean dynamic_viewport)
{
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);
  priv->renderer = g_object_ref (renderer);
  GulkanContext *gulkan = gulkan_renderer_get_context (renderer);
  GulkanDevice *gulkan_device = gulkan_context_get_device (gulkan);

  priv->transformation_ubo = gulkan_uniform_buffer_new (gulkan_device,
                                                        sizeof (
                                                          Transformation));
  if (!priv->transformation_ubo)
    return FALSE;

  gulkan_uniform_buffer_update (priv->transformation_ubo,
                                (gpointer) &priv->ubo);

  VkDescriptorSetLayoutBinding bindings[] = {
    {
      .binding = 0,
      .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
      .descriptorCount = 1,
      .stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
      .pImmutableSamplers = NULL,
    },
    {
      .binding = 1,
      .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
      .descriptorCount = 1,
      .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
    },
  };

  priv->descriptor_pool = GULKAN_DESCRIPTOR_POOL_NEW (gulkan, bindings, 1);
  if (!priv->descriptor_pool)
    return FALSE;

  priv->descriptor_set = gulkan_descriptor_pool_create_set (
    priv->descriptor_pool);

  gulkan_descriptor_set_update_buffer (priv->descriptor_set, 0,
                                       priv->transformation_ubo);

  SphvrPipelineClass *klass = SPHVR_PIPELINE_GET_CLASS (self);
  if (klass->init_vertex_buffer == NULL) {
    printf ("Error: could not get vertex buffer func.\n");
    return FALSE;
  }

  priv->vb = klass->init_vertex_buffer (self, renderer);
  if (!priv->vb) {
    printf ("Error: could not init vertex buffer.\n");
    return FALSE;
  }

  if (klass->init_pipeline == NULL) {
    return FALSE;
  }

  priv->pipeline = klass->init_pipeline (self, renderer, priv->descriptor_pool,
                                         render_pass, priv->vb,
                                         dynamic_viewport);

  if (!priv->pipeline) {
    return FALSE;
  }

  return TRUE;
}

static void
sphvr_pipeline_class_init (SphvrPipelineClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

GulkanPipeline *
sphvr_pipeline_get_pipeline (SphvrPipeline *self)
{
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);
  return priv->pipeline;
}

GulkanDescriptorPool *
sphvr_pipeline_get_descriptor_pool (SphvrPipeline *self)
{
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);
  return priv->descriptor_pool;
}

GulkanDescriptorSet *
sphvr_pipeline_get_descriptor_set (SphvrPipeline *self)
{
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);
  return priv->descriptor_set;
}

GulkanVertexBuffer *
sphvr_pipeline_get_vb (SphvrPipeline *self)
{
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);
  return priv->vb;
}

sphvr_projection_type
sphvr_pipeline_get_projection_type (SphvrPipeline *self)
{
  SphvrPipelinePrivate *priv = sphvr_pipeline_get_instance_private (self);
  return priv->projection_type;
}

/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

typedef enum
{
  SPHVR_STEREO_NONE,
  SPHVR_STEREO_SBS,
  SPHVR_STEREO_TB,
} sphvr_stereo_mode;

typedef enum
{
  SPHVR_φ_360,
  SPHVR_φ_180,
} sphvr_φ_range;

typedef enum
{
  SPHVR_PROJECTION_EQUIRECT,
  SPHVR_PROJECTION_CUBE,
  SPHVR_PROJECTION_EAC
} sphvr_projection_type;

typedef enum
{
  SPHVR_OUTPUT_DESKTOP,
  SPHVR_OUTPUT_XR
} sphvr_output_type;

const char *
sphvr_stereo_mode_str (sphvr_stereo_mode code);

const char *
sphvr_φ_range_str (sphvr_φ_range code);

const char *
sphvr_projection_type_str (sphvr_projection_type code);

const char *
sphvr_output_type_str (sphvr_output_type code);

/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "sphvr-cube.h"
#include "sphvr-pipeline.h"

struct _SphvrPipelineCube
{
  SphvrPipeline parent;
};

G_DEFINE_TYPE (SphvrPipelineCube, sphvr_pipeline_cube, SPHVR_TYPE_PIPELINE)

static void
sphvr_pipeline_cube_init (SphvrPipelineCube *self)
{
  (void) self;
}

static GulkanPipeline *
_init_pipeline (SphvrPipeline *pipeline, GulkanRenderer *renderer,
                GulkanDescriptorPool *descriptor_pool,
                GulkanRenderPass *render_pass, GulkanVertexBuffer *vb,
                gboolean dynamic_viewport)
{
  SphvrPipelineCube *self = SPHVR_PIPELINE_CUBE (pipeline);

  const char *frag_shader_path;
  switch (sphvr_pipeline_get_projection_type (SPHVR_PIPELINE (self))) {
    case SPHVR_PROJECTION_CUBE:
      frag_shader_path = "/shaders/cubemap.frag.spv";
      break;
    default:
      frag_shader_path = "/shaders/eac.frag.spv";
  }

  VkVertexInputBindingDescription *binding_desc
    = gulkan_vertex_buffer_create_binding_desc (vb);
  VkVertexInputAttributeDescription *attrib_desc
    = gulkan_vertex_buffer_create_attrib_desc (vb);

  GulkanPipelineConfig config = {
    .extent = gulkan_renderer_get_extent (renderer),
    .sample_count = VK_SAMPLE_COUNT_1_BIT,
    .vertex_shader_uri = "/shaders/cubemap.vert.spv",
    .fragment_shader_uri = frag_shader_path,
    .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,
    .attribs = attrib_desc,
    .attrib_count = gulkan_vertex_buffer_get_attrib_count (vb),
    .bindings = binding_desc,
    .binding_count = gulkan_vertex_buffer_get_attrib_count (vb),
    .blend_attachments = (VkPipelineColorBlendAttachmentState[]){
      {
        .colorWriteMask =
          VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
          VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
        },
      },
    .rasterization_state = &(VkPipelineRasterizationStateCreateInfo){
      .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
      .polygonMode = VK_POLYGON_MODE_FILL,
      .cullMode = VK_CULL_MODE_NONE,
      .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
      .lineWidth = 1.0f,
    },
    .dynamic_viewport = dynamic_viewport,
  };

  GulkanContext *gulkan = gulkan_renderer_get_context (renderer);
  GulkanPipeline *gulkan_pipeline = gulkan_pipeline_new (gulkan,
                                                         descriptor_pool,
                                                         render_pass, &config);

  g_free (binding_desc);
  g_free (attrib_desc);

  return gulkan_pipeline;
}

static GulkanVertexBuffer *
_init_vertex_buffer (SphvrPipeline *pipeline, GulkanRenderer *renderer)
{
  SphvrPipelineCube *self = SPHVR_PIPELINE_CUBE (pipeline);
  GulkanContext *gulkan = gulkan_renderer_get_context (renderer);
  GulkanDevice *device = gulkan_context_get_device (gulkan);

  GulkanVertexBuffer *vb
    = gulkan_vertex_buffer_new (device, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP);

  switch (sphvr_pipeline_get_projection_type (SPHVR_PIPELINE (self))) {
    case SPHVR_PROJECTION_CUBE: {
      gulkan_vertex_buffer_add_attribute (vb, 3, sizeof (positions), 0,
                                          (uint8_t *) positions);
      gulkan_vertex_buffer_add_attribute (vb, 2, sizeof (uvs), 0,
                                          (uint8_t *) uvs);
      gulkan_vertex_buffer_add_attribute (vb, 2, sizeof (offsets), 0,
                                          (uint8_t *) offsets);
      break;
    }
    default: {
      gulkan_vertex_buffer_add_attribute (vb, 3, sizeof (positions), 0,
                                          (uint8_t *) positions);
      gulkan_vertex_buffer_add_attribute (vb, 2, sizeof (uvs_eac), 0,
                                          (uint8_t *) uvs_eac);
      gulkan_vertex_buffer_add_attribute (vb, 2, sizeof (offsets_eac), 0,
                                          (uint8_t *) offsets_eac);
      break;
    }
  }

  if (!gulkan_vertex_buffer_upload (vb))
    return FALSE;

  if (!vb)
    return NULL;

  return vb;
}

static void
_draw_cmd (GulkanVertexBuffer *vb, VkCommandBuffer cmd_buffer)
{
  gulkan_vertex_buffer_bind_with_offsets (vb, cmd_buffer);
  vkCmdDraw (cmd_buffer, 4, 1, 0, 0);
  vkCmdDraw (cmd_buffer, 4, 1, 4, 0);
  vkCmdDraw (cmd_buffer, 4, 1, 8, 0);
  vkCmdDraw (cmd_buffer, 4, 1, 12, 0);
  vkCmdDraw (cmd_buffer, 4, 1, 16, 0);
  vkCmdDraw (cmd_buffer, 4, 1, 20, 0);
}

static void
sphvr_pipeline_cube_class_init (SphvrPipelineCubeClass *klass)
{
  SphvrPipelineClass *sphvr_pipeline_class = SPHVR_PIPELINE_CLASS (klass);
  sphvr_pipeline_class->init_vertex_buffer = _init_vertex_buffer;
  sphvr_pipeline_class->draw_cmd = _draw_cmd;
  sphvr_pipeline_class->init_pipeline = _init_pipeline;
}

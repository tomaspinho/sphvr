/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <glib.h>
#include <graphene.h>

G_BEGIN_DECLS

void
glm_sphere_view (float theta, float phi, graphene_matrix_t *out);

G_END_DECLS

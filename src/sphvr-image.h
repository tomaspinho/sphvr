/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <glib-object.h>
#include <gulkan.h>
#include <stdbool.h>

#define SPHVR_TYPE_IMAGE sphvr_image_get_type ()
G_DECLARE_FINAL_TYPE (SphvrImage, sphvr_image, SPHVR, IMAGE, GObject)

SphvrImage *
sphvr_image_new (void);

bool
sphvr_image_load_from_path (SphvrImage *self, const char *path);

bool
sphvr_image_load_from_res (SphvrImage *self, const char *path);

bool
sphvr_image_init_texture (SphvrImage *self, GulkanContext *client);

VkImageView
sphvr_image_get_view (SphvrImage *self);

VkSampler
sphvr_image_get_sampler (SphvrImage *self);

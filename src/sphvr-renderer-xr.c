/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include <gxr.h>

#include "glm-math.h"
#include "sphvr-math.h"
#include "sphvr-pipeline.h"
#include "sphvr-renderer.h"

struct _SphvrRendererXr
{
  GulkanRenderer parent;

  GxrContext *context;

  GulkanRenderPass *render_pass;
  GulkanCmdBuffer **cmd_buffers;

  SphvrPipeline *pipeline;
};

static void
sphvr_renderer_xr_interface_init (SphvrRendererInterface *iface);

G_DEFINE_TYPE_WITH_CODE (
  SphvrRendererXr, sphvr_renderer_xr, GULKAN_TYPE_RENDERER,
  G_IMPLEMENT_INTERFACE (SPHVR_TYPE_RENDERER,
                         sphvr_renderer_xr_interface_init))

static void
sphvr_renderer_xr_init (SphvrRendererXr *self)
{
  self->context = NULL;
  self->cmd_buffers = NULL;
  self->pipeline = NULL;
}

static GxrContext *
_create_gxr_context ()
{
  GSList *instance_ext_list
    = gulkan_context_get_external_memory_instance_extensions ();

  GSList *device_ext_list
    = gulkan_context_get_external_memory_device_extensions ();

  GxrContext *context
    = gxr_context_new_from_vulkan_extensions (instance_ext_list,
                                              device_ext_list, "SPHVR", 1);

  g_slist_free_full (instance_ext_list, g_free);
  g_slist_free_full (device_ext_list, g_free);
  return context;
}

static void
_render_stereo (SphvrRendererXr *self, VkCommandBuffer cmd_buffer,
                uint32_t swapchain_image_id)
{
  VkExtent2D extent = gulkan_renderer_get_extent (GULKAN_RENDERER (self));

  VkClearColorValue clear_color = {.float32 = {0, 0, 0, 0}};

  GulkanFrameBuffer *framebuffer
    = gxr_context_get_framebuffer_at (self->context, swapchain_image_id);

  gulkan_render_pass_begin (self->render_pass, extent, clear_color,
                            framebuffer, cmd_buffer);

  sphvr_pipeline_init_draw_cmd (self->pipeline, cmd_buffer);

  vkCmdEndRenderPass (cmd_buffer);
}

static gboolean
_initialize (SphvrRenderer *renderer)
{
  SphvrRendererXr *self = SPHVR_RENDERER_XR (renderer);
  self->context = _create_gxr_context ();
  if (!self->context)
    return FALSE;

  self->cmd_buffers = g_malloc (
    sizeof (GulkanCmdBuffer *)
    * gxr_context_get_swapchain_length (self->context));

  gulkan_renderer_set_context (GULKAN_RENDERER (self),
                               gxr_context_get_gulkan (self->context));

  VkExtent2D extent = gxr_context_get_swapchain_extent (self->context, 0);

  gulkan_renderer_set_extent (GULKAN_RENDERER (self), extent);

  if (!gxr_context_init_framebuffers (self->context, extent,
                                      VK_SAMPLE_COUNT_1_BIT,
                                      &self->render_pass))
    return FALSE;

  if (!sphvr_pipeline_initialize (self->pipeline, GULKAN_RENDERER (self),
                                  self->render_pass, FALSE)) {
    g_printerr ("Could not initialize pipeline.");
    return FALSE;
  }

  return TRUE;
}

static void
_update_transformation (SphvrRendererXr *self)
{
  graphene_matrix_t view_projections[2];

  for (uint32_t eye = 0; eye < 2; eye++) {
    graphene_matrix_t view;
    graphene_matrix_t projection;

    gxr_context_get_view (self->context, eye, &view);

    gxr_context_get_projection (self->context, eye, 0.01f, 1000.0f,
                                &projection);

    if (sphvr_pipeline_get_projection_type (self->pipeline)
        == SPHVR_PROJECTION_EQUIRECT) {
      sphvr_math_inverse_view_projection (&view, &projection,
                                          &view_projections[eye]);
    } else {
      graphene_matrix_t model;
      graphene_matrix_init_scale (&model, 500.0f, 500.0f, 500.0f);

      sphvr_math_fix_handedness (&view, &view);

      graphene_matrix_t model_view;
      graphene_matrix_multiply (&model, &view, &model_view);

      graphene_matrix_multiply (&model_view, &projection,
                                &view_projections[eye]);
    }
  }

  sphvr_pipeline_set_vps (self->pipeline, view_projections);
}

static void
_draw (SphvrRenderer *renderer)
{
  SphvrRendererXr *self = SPHVR_RENDERER_XR (renderer);
  GulkanContext *gc = gxr_context_get_gulkan (self->context);
  GulkanDevice *device = gulkan_context_get_device (gc);
  GulkanQueue *queue = gulkan_device_get_graphics_queue (device);

  gxr_context_poll_events (self->context);

  if (!gxr_context_begin_frame (self->context))
    return;

  _update_transformation (self);

  uint32_t i = gxr_context_get_buffer_index (self->context);

  gulkan_queue_submit (queue, self->cmd_buffers[i]);

  gxr_context_end_frame (self->context);
}

static void
_finalize (GObject *gobject)
{
  SphvrRendererXr *self = SPHVR_RENDERER_XR (gobject);
  GulkanContext *gc = gxr_context_get_gulkan (self->context);
  GulkanDevice *gd = gulkan_context_get_device (gc);

  VkDevice device = gulkan_context_get_device_handle (gc);
  GulkanQueue *queue = gulkan_device_get_graphics_queue (gd);

  vkDeviceWaitIdle (device);

  g_clear_object (&self->pipeline);

  uint32_t swapchain_length = gxr_context_get_swapchain_length (self->context);
  for (uint32_t i = 0; i < swapchain_length; i++) {
    gulkan_queue_free_cmd_buffer (queue, self->cmd_buffers[i]);
  }

  g_free (self->cmd_buffers);

  g_clear_object (&self->render_pass);
  g_clear_object (&self->context);

  G_OBJECT_CLASS (sphvr_renderer_xr_parent_class)->finalize (gobject);
}

static void
sphvr_renderer_xr_class_init (SphvrRendererXrClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

static GulkanRenderer *
_get_renderer (SphvrRenderer *sphvr_renderer)
{
  SphvrRendererXr *self = SPHVR_RENDERER_XR (sphvr_renderer);
  return GULKAN_RENDERER (self);
}

static SphvrPipeline *
_get_pipeline (SphvrRenderer *sphvr_renderer)
{
  SphvrRendererXr *self = SPHVR_RENDERER_XR (sphvr_renderer);
  return self->pipeline;
}

static void
_set_pipeline (SphvrRenderer *sphvr_renderer, SphvrPipeline *pipeline)
{
  SphvrRendererXr *self = SPHVR_RENDERER_XR (sphvr_renderer);
  self->pipeline = g_object_ref (pipeline);
}

static gboolean
_cache_draw_commands (SphvrRenderer *renderer)
{
  SphvrRendererXr *self = SPHVR_RENDERER_XR (renderer);
  GulkanContext *gc = gxr_context_get_gulkan (self->context);
  GulkanDevice *device = gulkan_context_get_device (gc);
  GulkanQueue *queue = gulkan_device_get_graphics_queue (device);

  uint32_t swapchain_length = gxr_context_get_swapchain_length (self->context);
  for (uint32_t i = 0; i < swapchain_length; i++) {
    self->cmd_buffers[i] = gulkan_queue_request_cmd_buffer (queue);
    gulkan_cmd_buffer_begin (self->cmd_buffers[i], 0);

    VkCommandBuffer cmd_handle = gulkan_cmd_buffer_get_handle (
      self->cmd_buffers[i]);
    _render_stereo (self, cmd_handle, i);
    gulkan_cmd_buffer_end (self->cmd_buffers[i]);
  }
  return TRUE;
}

static void
sphvr_renderer_xr_interface_init (SphvrRendererInterface *iface)
{
  iface->get_renderer = _get_renderer;
  iface->get_pipeline = _get_pipeline;
  iface->set_pipeline = _set_pipeline;
  iface->cache_draw_commands = _cache_draw_commands;
  iface->draw = _draw;
  iface->initialize = _initialize;
}

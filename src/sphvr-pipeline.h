/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <glib-object.h>
#include <gulkan.h>

#include "sphvr-enums.h"

#define SPHVR_TYPE_PIPELINE sphvr_pipeline_get_type ()
G_DECLARE_DERIVABLE_TYPE (SphvrPipeline, sphvr_pipeline, SPHVR, PIPELINE,
                          GObject)

struct _SphvrPipelineClass
{
  GObjectClass parent;

  GulkanVertexBuffer *(*init_vertex_buffer) (SphvrPipeline *self,
                                             GulkanRenderer *renderer);

  void (*draw_cmd) (GulkanVertexBuffer *vb, VkCommandBuffer cmd_buffer);

  GulkanPipeline *(*init_pipeline) (SphvrPipeline *pipeline,
                                    GulkanRenderer *renderer,
                                    GulkanDescriptorPool *descriptor_pool,
                                    GulkanRenderPass *render_pass,
                                    GulkanVertexBuffer *vb,
                                    gboolean dynamic_viewport);
};

#define SPHVR_TYPE_PIPELINE_CUBE sphvr_pipeline_cube_get_type ()
G_DECLARE_FINAL_TYPE (SphvrPipelineCube, sphvr_pipeline_cube, SPHVR,
                      PIPELINE_CUBE, SphvrPipeline)

#define SPHVR_TYPE_PIPELINE_PLANE sphvr_pipeline_plane_get_type ()
G_DECLARE_FINAL_TYPE (SphvrPipelinePlane, sphvr_pipeline_plane, SPHVR,
                      PIPELINE_PLANE, SphvrPipeline)

SphvrPipeline *
sphvr_pipeline_new (sphvr_projection_type projection_type, float gamma,
                    sphvr_stereo_mode stereo_mode, sphvr_φ_range φ_range);

gboolean
sphvr_pipeline_initialize (SphvrPipeline *self, GulkanRenderer *renderer,
                           GulkanRenderPass *render_pass,
                           gboolean dynamic_viewport);

void
sphvr_pipeline_set_image_view (SphvrPipeline *self,
                               VkImageView texture_image_view,
                               VkSampler sampler);

void
sphvr_pipeline_set_vp (SphvrPipeline *self, graphene_matrix_t *vp);

void
sphvr_pipeline_set_vps (SphvrPipeline *self, graphene_matrix_t *vp);

void
sphvr_pipeline_init_draw_cmd (SphvrPipeline *self, VkCommandBuffer cmd_buffer);

GulkanPipeline *
sphvr_pipeline_get_pipeline (SphvrPipeline *self);

GulkanDescriptorPool *
sphvr_pipeline_get_descriptor_pool (SphvrPipeline *self);

GulkanDescriptorSet *
sphvr_pipeline_get_descriptor_set (SphvrPipeline *self);

GulkanVertexBuffer *
sphvr_pipeline_get_vb (SphvrPipeline *self);

sphvr_projection_type
sphvr_pipeline_get_projection_type (SphvrPipeline *self);

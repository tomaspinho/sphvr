/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include <stdio.h>

#include "sphvr-camera-desktop.h"

#include "glm-math.h"
#include "sphvr-math.h"

#define M_PI 3.14159265358979323846

G_DEFINE_TYPE (SphvrCameraDesktop, sphvr_camera_desktop, G_TYPE_OBJECT)

static void
sphvr_camera_desktop_init (SphvrCameraDesktop *self)
{
  self->fov = 45.0;
  self->aspect = 16.0 / 9.0;
  self->znear = 0.01;
  self->zfar = 1000.0;

  graphene_vec3_init (&self->eye, 0.f, 0.f, 1.f);
  graphene_vec3_init (&self->center, 0.f, 0.f, 0.f);
  graphene_vec3_init (&self->up, 0.f, 1.f, 0.f);

  self->cursor_last_x = 0;
  self->cursor_last_y = 0;
  self->pressed_mouse_button = 0;

  self->center_distance = 2.5;
  self->scroll_speed = 0.03;
  self->rotation_speed = 0.002;

  self->theta = 1.5 * M_PI;
  self->phi = 0.0;

  self->zoom_step = 0.95;
  self->min_fov = 5;
  self->max_fov = 150;

  self->mouse_pressed = false;

  self->inverse_mode = false;

  sphvr_camera_desktop_update_view (self);
}

SphvrCameraDesktop *
sphvr_camera_desktop_new (void)
{
  return (SphvrCameraDesktop *) g_object_new (SPHVR_TYPE_CAMERA_DESKTOP, 0);
}

static void
_finalize (GObject *gobject)
{
  SphvrCameraDesktop *self = SPHVR_CAMERA_DESKTOP (gobject);
  (void) self;
  G_OBJECT_CLASS (sphvr_camera_desktop_parent_class)->finalize (gobject);
}

static void
sphvr_camera_desktop_class_init (SphvrCameraDesktopClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;
}

void
sphvr_camera_desktop_update_view (SphvrCameraDesktop *self)
{
  graphene_matrix_t projection_matrix;
  graphene_matrix_init_perspective (&projection_matrix, self->fov,
                                    self->aspect, self->znear, self->zfar);

  graphene_matrix_t view_matrix;
  /* TODO: Fix graphene */
  // graphene_matrix_init_look_at (&view_matrix, &self->center, &self->eye,
  //    &self->up);
  glm_sphere_view (self->theta, self->phi, &view_matrix);

  if (self->inverse_mode) {
    sphvr_math_inverse_view_projection (&view_matrix, &projection_matrix,
                                        &self->mvp);
  } else {
    graphene_matrix_multiply (&view_matrix, &projection_matrix, &self->mvp);
  }
}

void
sphvr_camera_desktop_translate (SphvrCameraDesktop *self, float z)
{
  float new_val = self->center_distance + z * self->scroll_speed;

  self->center_distance = MAX (0, new_val);

  g_print ("center distance: %f", self->center_distance);
  sphvr_camera_desktop_update_view (self);
}

void
_rotate (SphvrCameraDesktop *self, gdouble x, gdouble y)
{
  float delta_theta = y * self->rotation_speed;
  float delta_phi = x * self->rotation_speed;

  self->phi += delta_phi;

  /* 2π < θ < π to avoid gimbal lock */
  float next_theta_pi = (self->theta + delta_theta) / M_PI;
  if (next_theta_pi < 2.0 && next_theta_pi > 1.0)
    self->theta += delta_theta;

  // printf ("θ = %fπ ϕ = %fπ\n", self->theta / M_PI, self->phi / M_PI);
  sphvr_camera_desktop_update_view (self);
}

void
sphvr_camera_desktop_set_cursor_position (SphvrCameraDesktop *self, gdouble x,
                                          gdouble y)
{
  gdouble dx, dy;
  dx = x - self->cursor_last_x;
  dy = y - self->cursor_last_y;

  if (self->mouse_pressed) {
    _rotate (self, dx, dy);
  }
  self->cursor_last_x = x;
  self->cursor_last_y = y;
}

/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "sphvr-renderer.h"
#include "sphvr-pipeline.h"

G_DEFINE_INTERFACE (SphvrRenderer, sphvr_renderer, G_TYPE_OBJECT)

static void
sphvr_renderer_default_init (SphvrRendererInterface *iface)
{
  (void) iface;
}

void
sphvr_renderer_draw (SphvrRenderer *self)
{
  SphvrRendererInterface *iface = SPHVR_RENDERER_GET_IFACE (self);
  iface->draw (self);
}

SphvrPipeline *
sphvr_renderer_get_pipeline (SphvrRenderer *self)
{
  SphvrRendererInterface *iface = SPHVR_RENDERER_GET_IFACE (self);
  return iface->get_pipeline (self);
}

void
sphvr_renderer_set_pipeline (SphvrRenderer *self, SphvrPipeline *pipeline)
{
  SphvrRendererInterface *iface = SPHVR_RENDERER_GET_IFACE (self);
  iface->set_pipeline (self, pipeline);
}

GulkanRenderer *
sphvr_renderer_get_renderer (SphvrRenderer *self)
{
  SphvrRendererInterface *iface = SPHVR_RENDERER_GET_IFACE (self);
  return iface->get_renderer (self);
}

gboolean
sphvr_renderer_cache_draw_commands (SphvrRenderer *self)
{
  SphvrRendererInterface *iface = SPHVR_RENDERER_GET_IFACE (self);
  return iface->cache_draw_commands (self);
}

gboolean
sphvr_renderer_initialize (SphvrRenderer *self)
{
  SphvrRendererInterface *iface = SPHVR_RENDERER_GET_IFACE (self);
  return iface->initialize (self);
}

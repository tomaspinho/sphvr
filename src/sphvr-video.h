/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <glib-object.h>
#include <stdbool.h>
#include <vulkan/vulkan.h>

#include "sphvr-renderer.h"

#define SPHVR_TYPE_VIDEO sphvr_video_get_type ()
G_DECLARE_FINAL_TYPE (SphvrVideo, sphvr_video, SPHVR, VIDEO, GObject)

SphvrVideo *
sphvr_video_new (void);

void
sphvr_video_set_renderer (SphvrVideo *self, SphvrRenderer *renderer);

bool
sphvr_video_initialize (SphvrVideo *self, const char *uri);

void
sphvr_video_toggle_pause (SphvrVideo *self);

void
sphvr_video_seek (SphvrVideo *self, int seconds);

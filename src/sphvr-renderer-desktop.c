/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "sphvr-pipeline.h"
#include "sphvr-renderer.h"

struct _SphvrRendererDesktop
{
  GulkanSwapchainRenderer parent;

  SphvrPipeline *pipeline;
};

enum
{
  PIPELINE_INIT_SIGNAL,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = {0};

static void
sphvr_renderer_desktop_interface_init (SphvrRendererInterface *iface);

G_DEFINE_TYPE_WITH_CODE (
  SphvrRendererDesktop, sphvr_renderer_desktop, GULKAN_TYPE_SWAPCHAIN_RENDERER,
  G_IMPLEMENT_INTERFACE (SPHVR_TYPE_RENDERER,
                         sphvr_renderer_desktop_interface_init))

static void
sphvr_renderer_desktop_init (SphvrRendererDesktop *self)
{
  self->pipeline = NULL;
}

static void
_finalize (GObject *gobject)
{
  SphvrRendererDesktop *self = SPHVR_RENDERER_DESKTOP (gobject);
  g_clear_object (&self->pipeline);
  G_OBJECT_CLASS (sphvr_renderer_desktop_parent_class)->finalize (gobject);
}

static void
_init_draw_cmd (GulkanSwapchainRenderer *renderer, VkCommandBuffer cmd_buffer)
{
  SphvrRendererDesktop *self = SPHVR_RENDERER_DESKTOP (renderer);
  sphvr_pipeline_init_draw_cmd (self->pipeline, cmd_buffer);
}

static gboolean
_init_pipeline (GulkanSwapchainRenderer *renderer, gconstpointer data)
{
  (void) data;
  SphvrRendererDesktop *self = SPHVR_RENDERER_DESKTOP (renderer);

  GulkanRenderPass *render_pass = gulkan_swapchain_renderer_get_render_pass (
    renderer);

  if (!sphvr_pipeline_initialize (self->pipeline, GULKAN_RENDERER (self),
                                  render_pass, TRUE)) {
    g_printerr ("Could not init pipeline.\n");
    return FALSE;
  }

  g_signal_emit (self, signals[PIPELINE_INIT_SIGNAL], 0);

  return TRUE;
}

static void
sphvr_renderer_desktop_class_init (SphvrRendererDesktopClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->finalize = _finalize;

  GulkanSwapchainRendererClass *parent_class
    = GULKAN_SWAPCHAIN_RENDERER_CLASS (klass);
  parent_class->init_draw_cmd = _init_draw_cmd;
  parent_class->init_pipeline = _init_pipeline;

  signals[PIPELINE_INIT_SIGNAL] = g_signal_new ("pipeline-init",
                                                G_TYPE_FROM_CLASS (klass),
                                                G_SIGNAL_RUN_FIRST, 0, NULL,
                                                NULL, NULL, G_TYPE_NONE, 0);
}

static GulkanRenderer *
_get_renderer (SphvrRenderer *sphvr_renderer)
{
  SphvrRendererDesktop *self = SPHVR_RENDERER_DESKTOP (sphvr_renderer);
  return GULKAN_RENDERER (self);
}

static SphvrPipeline *
_get_pipeline (SphvrRenderer *sphvr_renderer)
{
  SphvrRendererDesktop *self = SPHVR_RENDERER_DESKTOP (sphvr_renderer);
  return self->pipeline;
}

static void
_set_pipeline (SphvrRenderer *sphvr_renderer, SphvrPipeline *pipeline)
{
  SphvrRendererDesktop *self = SPHVR_RENDERER_DESKTOP (sphvr_renderer);
  self->pipeline = g_object_ref (pipeline);
}

static void
_draw (SphvrRenderer *self)
{
  GulkanRenderer *renderer = sphvr_renderer_get_renderer (self);
  gulkan_renderer_draw (renderer);

  GulkanContext *gulkan = gulkan_renderer_get_context (renderer);
  gulkan_device_wait_idle (gulkan_context_get_device (gulkan));
}

static gboolean
_cache_draw_commands (SphvrRenderer *renderer)
{
  SphvrRendererDesktop *self = SPHVR_RENDERER_DESKTOP (renderer);
  return gulkan_swapchain_renderer_init_draw_cmd_buffers (
    GULKAN_SWAPCHAIN_RENDERER (self));
}

static gboolean
_initialize (SphvrRenderer *renderer)
{
  VkClearColorValue black = {
    .float32 = {0.0f, 0.0f, 0.0f, 1.0f},
  };

  GulkanRenderer *r = sphvr_renderer_get_renderer (renderer);
  GulkanSwapchainRenderer *scr = GULKAN_SWAPCHAIN_RENDERER (r);

  gulkan_swapchain_renderer_initialize (scr, black, NULL);

  return TRUE;
}

static void
sphvr_renderer_desktop_interface_init (SphvrRendererInterface *iface)
{
  iface->get_renderer = _get_renderer;
  iface->get_pipeline = _get_pipeline;
  iface->set_pipeline = _set_pipeline;
  iface->cache_draw_commands = _cache_draw_commands;
  iface->draw = _draw;
  iface->initialize = _initialize;
}

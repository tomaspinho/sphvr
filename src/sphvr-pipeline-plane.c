/*
 * sphvr
 * Copyright 2021 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#include "sphvr-pipeline.h"

typedef struct
{
  float position[2];
  float uv[2];
} Vertex;

// clang-format off
static const Vertex vertices[4] = {
  {{-1.f, -1.f}, {1.f, 0.f}},
  {{ 1.f, -1.f}, {0.f, 0.f}},
  {{ 1.f,  1.f}, {0.f, 1.f}},
  {{-1.f,  1.f}, {1.f, 1.f}}
};
// clang-format on

static const uint16_t indices[6] = {0, 1, 2, 2, 3, 0};

struct _SphvrPipelinePlane
{
  SphvrPipeline parent;
};

G_DEFINE_TYPE (SphvrPipelinePlane, sphvr_pipeline_plane, SPHVR_TYPE_PIPELINE)

static void
sphvr_pipeline_plane_init (SphvrPipelinePlane *self)
{
  (void) self;
}

static GulkanPipeline *
_init_pipeline (SphvrPipeline *pipeline, GulkanRenderer *renderer,
                GulkanDescriptorPool *descriptor_pool,
                GulkanRenderPass *render_pass, GulkanVertexBuffer *vb,
                gboolean dynamic_viewport)
{
  (void) pipeline;
  (void) vb;

  VkVertexInputBindingDescription binding_desc[] = {
    {
      .binding = 0,
      .stride = sizeof (Vertex),
      .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
    },
  };
  VkVertexInputAttributeDescription attrib_desc[] = {
    {
      .location = 0,
      .binding = 0,
      .format = VK_FORMAT_R32G32_SFLOAT,
      .offset = offsetof (Vertex, position),
    },
    {
      .location = 1,
      .binding = 0,
      .format = VK_FORMAT_R32G32_SFLOAT,
      .offset = offsetof (Vertex, uv),
    },
  };

  GulkanPipelineConfig config = {
    .extent = gulkan_renderer_get_extent (renderer),
    .sample_count = VK_SAMPLE_COUNT_1_BIT,
    .vertex_shader_uri = "/shaders/equirect.vert.spv",
    .fragment_shader_uri =  "/shaders/equirect.frag.spv",
    .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
    .attribs = attrib_desc,
    .attrib_count = G_N_ELEMENTS (attrib_desc),
    .bindings = binding_desc,
    .binding_count = G_N_ELEMENTS (binding_desc),
    .blend_attachments = (VkPipelineColorBlendAttachmentState[]){
      {
        .colorWriteMask =
          VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
          VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
        },
      },
    .rasterization_state = &(VkPipelineRasterizationStateCreateInfo){
      .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
      .polygonMode = VK_POLYGON_MODE_FILL,
      .cullMode = VK_CULL_MODE_NONE,
      .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
      .lineWidth = 1.0f,
    },
    .dynamic_viewport = dynamic_viewport,
  };

  GulkanContext *gulkan = gulkan_renderer_get_context (renderer);
  GulkanPipeline *gulkan_pipeline = gulkan_pipeline_new (gulkan,
                                                         descriptor_pool,
                                                         render_pass, &config);

  return gulkan_pipeline;
}

static GulkanVertexBuffer *
_init_vertex_buffer (SphvrPipeline *self, GulkanRenderer *renderer)
{
  (void) self;
  GulkanContext *gulkan = gulkan_renderer_get_context (renderer);
  GulkanDevice *device = gulkan_context_get_device (gulkan);

  GulkanVertexBuffer *vb
    = gulkan_vertex_buffer_new (device, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
  if (!gulkan_vertex_buffer_alloc_data (vb, vertices, sizeof (vertices)))
    return NULL;

  if (!gulkan_vertex_buffer_alloc_index_data (vb, indices,
                                              VK_INDEX_TYPE_UINT16,
                                              G_N_ELEMENTS (indices)))
    return NULL;

  return vb;
}

static void
_draw_cmd (GulkanVertexBuffer *vb, VkCommandBuffer cmd_buffer)
{
  gulkan_vertex_buffer_draw_indexed (vb, cmd_buffer);
}

static void
sphvr_pipeline_plane_class_init (SphvrPipelinePlaneClass *klass)
{
  SphvrPipelineClass *sphvr_pipeline_class = SPHVR_PIPELINE_CLASS (klass);
  sphvr_pipeline_class->init_vertex_buffer = _init_vertex_buffer;
  sphvr_pipeline_class->draw_cmd = _draw_cmd;
  sphvr_pipeline_class->init_pipeline = _init_pipeline;
}
